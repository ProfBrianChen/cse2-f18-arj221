//////////////
//// CSE 02 Cyclometer
// Austen Johnson
// September 6th, 2018
// CSE 02 Lab 02
///
public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args){
    // given input data
    int secsTrip1 = 480; //Time of trip 1 in secs
    int secsTrip2 = 3220; // Time of trip 2 in secs
    int countsTrip1 = 1561; // Rotations of the front wheel for trip 1
    int countsTrip2 = 9037; // Rotations of the front wheel for trip 2
    
    double wheelDiameter = 27.0, //Diameter of wheel
    PI = 3.14159, //Given matematical constant, used to calculate distance
    feetPerMile = 5280, //Number of feet in a mile
    inchesPerFoot = 12, //Number of inches per foot
    secondsPerMinute = 60; //Number of seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //Make these 3 variables doubles
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); // Prints the time in minutes and the counts of trip 1 to the terminal screen
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); // Prints the time in minutes and the counts of trip 2 to the terminal screen
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //Total distance of trip 1 in inches 
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Gives distance in miles of trip 1
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // Given distance in miles of trip 2
    totalDistance = distanceTrip1 + distanceTrip2; // Total distance traveled between the 2 trips
    
    //Print out the data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");
    
    
    
  } //end of main method
} //end of class