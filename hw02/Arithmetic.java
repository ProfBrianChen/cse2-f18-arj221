//////////////
//// CSE 02 Arithmetic
// Austen Johnson
// September 9th, 2018
// CSE 02 HW 02
///
public class Arithmetic{
  //main method required for every Java program
  public static void main(String args[]){
    
    int numPants = 3; //Number of pairs of pant
    double pantsPrice = 34.98; //Cost per pair of pants

    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt

    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //cost per belt

    double paSalesTax = 0.06; //the tax rate in Pennsylvania
    
    double totalCostOfPants = (numPants * pantsPrice); //total cost of pants
    double totalCostOfShirts = (numShirts * shirtPrice); //total cost of shirts
    double totalCostOfBelt = (numBelts * beltCost); //total cost of belts
    
    double salesTaxPants = (totalCostOfPants * paSalesTax); //sales tax on the pants
    double salesTaxShirts = (totalCostOfShirts * paSalesTax); //sales tax on the shirts
    double salesTaxBelts = (totalCostOfBelt * paSalesTax); //sales tax on belts
    
    salesTaxPants = salesTaxPants * 100; //Multiplying sales tax by 100 to set up switch to integer
    salesTaxPants = (int) salesTaxPants; //Converting sales tax to an integer
    salesTaxPants =(salesTaxPants / 100.00); //Switching sales tax to an integer and then dividing by 100
    
    salesTaxShirts = salesTaxShirts * 100; //Multiplying sales tax by 100 to set up switch to integer
    salesTaxShirts = (int) salesTaxShirts; //Converting sales tax to an integer
    salesTaxShirts =(salesTaxShirts / 100.00); //Switching sales tax to an integer and then dividing by 100
    
    salesTaxBelts = salesTaxBelts * 100; //Multiplying sales tax by 100 to set up switch to integer
    salesTaxBelts = (int) salesTaxBelts; //Converting sales tax to an integer
    salesTaxBelts =(salesTaxBelts / 100.00); //Switching sales tax to an integer and then dividing by 100
    
    double totalSalesTax = (salesTaxPants + salesTaxShirts + salesTaxBelts); //Total tax of purchased items
    
    double totalCostOfPurchases = (totalCostOfPants + totalCostOfShirts + totalCostOfBelt); //Total cost of purchases before sales tax
    
    double totalTransaction = (totalCostOfPurchases + totalSalesTax); //Total cost of each item and sales tax
    
    System.out.println("Total cost of pants: $" + totalCostOfPants); //Printing total cost of pants to terminal screen
    System.out.println("Total cost of shirts: $" + totalCostOfShirts); //Prints total cost of shirts to terminal screen
    System.out.println("Total cost of belts: $" + totalCostOfBelt); //Prints total cost of belts to terminal sceen
    System.out.println("Sales tax on pants: $" + salesTaxPants); //Prints sales tax on pants to terminal screen
    System.out.println("Sales tax on shirts: $" + salesTaxShirts); //Prints sales tax on shirts to terminal screen
    System.out.println("Sales tax on belts: $" + salesTaxBelts); //Prints sales tax on belts to terminal screen
    System.out.println("Total cost of purchases: $" + totalCostOfPurchases); //Prints total cost of purchases to terminal screen
    System.out.println("Total sales tax: $" + totalSalesTax); //Prints total sales tax to terminal screen
    System.out.println("Total cost of transaction: $" + totalTransaction); //Prints toal cost of transacion to terminal screen  
  }
}