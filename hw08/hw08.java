// Austen Johnson 
// 10/21/18
// CSE 02 hw08
////////

import java.util.Scanner;

public class hw08{ 
public static void main(String[] args) { 

Scanner scan = new Scanner(System.in); //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 

int numCards = 5; 
int again = 1; 
int index = 51;

for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 

System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 

while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 


public static void printArray(String[] list){
  for(int i=0; i<list.length; i++){
    System.out.print(list[i] + " ");
  }
  System.out.println();
}

public static String[] shuffle(String[] list){
  String tempHolder = " ";
  int randVal = 0;
  for (int i=0; i<80; i++){
    randVal = (int) (Math.random() * 51 + 1);
    tempHolder = list[0];
    list[0] = list[randVal];
    list[randVal] = tempHolder;
  }
  System.out.println("Shuffled");
  return list;
}

public static String[] getHand(String[] list, int index, int numCards){
  String hand[] = new String [numCards];
  int j = 0;
  for (int i=index; i > (index - numCards); i--){
    hand[j] = list[i];
    j++;
  }
  System.out.println("Hand");
  return hand;
}

}                           
