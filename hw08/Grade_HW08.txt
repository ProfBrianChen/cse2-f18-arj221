Grade: 85
 
Compiles    				          20 pts
+compiles and works
Comments 				              10 pts
+5 Has header and nothing else
Method shuffle(list)		          20 pts
+Shuffles correctly
Method getHand(list,index, numCards)  20 pts 
+grabs numCards correctly 
Method printArray(list)               20 pts
+Prints correctly
Number of Cards                       10 pts
-10 Doesn't reshuffle cards when program runs out.