//////////////
//// CSE 02 Hellow World
// Austen Johnson
// August 30th, 2018
// CSE 02 Lab 01
///
public class HelloWorld{
  
  public static void main(String args[]){
     ///prints Hello, World to terminal window
    System.out.println("Hello, World");
  }
  
}