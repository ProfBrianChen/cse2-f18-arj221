//////////////
//// CSE 02 Convert
// Austen Johnson
// September 17th, 2018
// CSE 02 HW 03
///
import java.util.Scanner;
//Using a scanner to find area affected in acres and inches of rain dropped

public class Convert{
  //main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in ); // naming the scanner
    System.out.print("Enter the affected area in acres: "); //Pronpting the user for affected area in acres
    double acresOfLand = myScanner.nextDouble(); // allows user to input the affected area in acres
    System.out.print("Enter the rainfall int the affected area in inches "); //prompting the user for affected area in inches
    double amountOfRainfall = myScanner.nextDouble(); // allows user to input the amount of rainfall in inches
    double acreInch = acresOfLand * amountOfRainfall; //finds the number of acres times inches to start the conversion to cubic miles
    double acreGallon = 27154.2859; //constant for an inch of rain fallen on one acre converted to gallons
    double gallons = acreInch * acreGallon; //finds the number of gallons dropped
    double milesToGallons = 1.101E12; //the number of gallons in a cubic mile
    double miles = (gallons / milesToGallons); //divides gallons by gallons in a mile to get cubic miles
    System.out.println(miles + " cubic miles"); //this is the number of cubic miles of rain
  }
}