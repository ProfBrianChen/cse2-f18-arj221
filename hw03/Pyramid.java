//////////////
//// CSE 02 Pyramid
// Austen Johnson
// September 17th, 2018
// CSE 02 HW 03
///
import java.util.Scanner;
//Using a scanner to find area affected in acres and inches of rain dropped

public class Pyramid{
  //main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner ( System.in ); // naming the scanner
    System.out.print("Enter the square side of the pyramid: "); //prompts user to input square of the side
    double pyramidSide = myScanner.nextDouble(); //alows user to input square side
    System.out.print("Enter the height of the pyramid: "); //prompts user to input height of the pyramid
    double pyramidHeight = myScanner.nextDouble(); //allows user to inpt height of pyramid
    double pyramidWidth = pyramidSide * pyramidSide; //find pyramid widt by doubling pyramid side
    double pyramidVolume = (pyramidWidth * pyramidHeight)/(3); //finds the pyramid volume
    System.out.println("Volume inside the pyramid is: " + pyramidVolume); //prints the pyramid volume to the terminal screen   
  }
}
    