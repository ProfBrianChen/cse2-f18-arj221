//////////////
//// CSE 02 Welcome CLass
// Austen Johnson
// September 4th, 2018
// CSE 02 HW 01
///
public class WelcomeClass{
  
  public static void main(String args[]){
    System.out.println("  -----------");
    /// prints 11 dashes for spacing to terminal screen
      System.out.println("  | WELCOME |");
      /// prints WELCOME to terminal screen
      System.out.println("  -----------");
     /// prints 11 dashes for spacing to terminal screen
      System.out.println("  ^  ^  ^  ^  ^  ^");
      /// prints 6 arrows to terminal screen
      System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
      /// prints 6 forward and backward slashes to terminal screen
      System.out.println("<-A--R--J--2--2--1->");
      /// prints my Lehigh network ID to terminal screen
      System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
      /// prints 6 forward and backward slashes to terminal screen
      System.out.println("  v  v  v  v  v  v");
      /// prints 6 v's to terminal screen  
      System.out.println("My name is Austen and I am a sophomore at Lehigh University."); 
      /// An autobiographical statement                   
  }
  
}