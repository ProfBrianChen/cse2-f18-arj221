//////////////
//// CSE 02 CardGenerator
// Austen Johnson
// September 20th, 2018
// CSE 02 Lab 04
///
import java.lang.Math; // importing the math class to use math functions in the main method
public class CardGenerator{
  //main method required for every Java program
  public static void main(String[] args) {
    
    int randNum = (int)(Math.random()*(51+1))+1; //making a random generator that counts from 1 to 52 emulating a deck of cards
    
    String suitName = ""; // identifying the string for the suit name
    String cardIdentity = ""; // identifying the string for the card identity
    
   if ((randNum >= 1) && (randNum <= 13)){
     suitName = "Diamonds";
   }// naming the cards 1-13 the Diamonds suit
    else if ((randNum >= 14) && (randNum <= 26)){
      suitName = "Clubs";
    } // naming the cards 14-26 the Clubs suit
    else if ((randNum >= 27) && (randNum <= 39)){
      suitName = "Hearts";
    } // naming the cards 27-39 the Hearts suit
    else if ((randNum >= 40) && (randNum <= 52)){
      suitName = "Spades";
    } // naming the cards 40-52 tp the Clubs suit
    
    switch (randNum % 13) {
      case 0: cardIdentity = "King"; // starting the switch statement for every card that is not a number ie Ace or King
        break;
      case 1: cardIdentity = "Ace";
        break;
      case 11: cardIdentity = "Jack";
        break;
      case 12: cardIdentity = "Queen";
        break;
    
    }
    if ((randNum % 13)>=2 && (randNum % 13)<=10){
      System.out.println ("You picked the " + (randNum % 13) + " of " + suitName + ".");
    }
    else {
    System.out.println("You picked the " + cardIdentity + " of " + suitName + ".");
    }
  }
}