//////////////
//// CSE 02 CrapsSwitch
// Austen Johnson
// September 24th, 2018
// CSE 02 HW 04
///
import java.util.Random; // importing the math class to use math functions in the main method
import java.util.Scanner; // importing the scanner class to use scanner functions to ask for inputs
public class CrapsSwitch{
  //main method required for every Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //name scanner
    Random numGen = new Random(); //random number generator
    
    System.out.println("Write '1' if you would like the dice to be selected randomly. Write '2' if you would like to choose your values."); //Asking the user for their input in how the dice numbers will be
    int selectDice = myScanner.nextInt(); //processes user input and assigns the value chosen
    
    int die1 = 0; // assign the dice variable
    int die2 = 0;
    String crapsTerms; //names of the craps terms
    
    // switch statement based on user decision of random dice roll or their own input of dice values
    switch (selectDice){
      case 1: // if user inputs '1'
        die1 = numGen.nextInt(6) + 1;// randomly generates 2 integers 1-6 for the dice
        die2 = numGen.nextInt(6) + 1;
        System.out.println("The two outcomes from the dice are " + die1 + " and " + die2 + "."); // prints the values of the dice rolls
        break;
      case 2: // if user inputs '2', then this branch is taken
        System.out.print("Enter the value of one die roll: "); // prints prompt for user to input one of the die values
        die1 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
        System.out.print("Enter the value of the other die roll: "); // prints prompt for user to input the other of the die values
        die2 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
        break;
      default: System.out.println("Invalid entry."); // if the user inputs anything other than 1 or 2, this is taken and prints that it is invalid
        break;
    }    
    int dieSum = die1 + die2; // the sum of the two die values

    switch(die1){ // used to keep range of die1 values to dice values 1-6
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6: die1 = die1;
        break; // break is here because for all values 1-6, the roll value will be kept as it is
      default: dieSum = 0; // used to invalidate any roll entry outside of 1-6
        System.out.println("Invalid entry.");
        break;
    }
    switch(die2){ // used to keep range of die2 values to dice values 1-6
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6: die2 = die2;
        break; // break is here because for all values 1-6, the die value will be kept as it is
      default: dieSum = 0; // used to invalidate any die entry outside of 1-6
        System.out.println("Invalid entry.");
        break;
    }
    
    // switch statement to assign the slang terminology to its dice values
    switch (dieSum){
      case 2: crapsTerms = "Snake Eyes"; // assigns slang term to a specific sum of the two dice
        System.out.println("This roll is called " + crapsTerms + "."); // prints statement stating the slang term
        break;
      case 3: crapsTerms = "Ace Deuce";
        System.out.println("This roll is called " + crapsTerms + ".");
        break;
      case 4:
        switch(die1){ // nested switch statement to differentiate the roll values for Hard vs. Easy slang terms
          case 2: crapsTerms= "Hard Four"; 
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
          default: crapsTerms = "Easy Four"; // for any other sum of 4 that is not 2 and 2, the name is different, which is why it's the default
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
        }
        break;
      case 5: crapsTerms = "Fever Five";
        System.out.println("This roll is called " + crapsTerms + ".");
        break;
      case 6:
        switch(die1){
          case 3: crapsTerms = "Hard Six";
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
          default: crapsTerms = "Easy Six";
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
        }
        break;
      case 7: crapsTerms = "Seven Out";
        System.out.println("This roll is called " + crapsTerms + ".");
        break;
      case 8:
        switch(die1){
          case 4: crapsTerms = "Hard Eight";
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
          default: crapsTerms = "Easy Eight";
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
        }
        break;
      case 9: crapsTerms = "Nine";
        System.out.println("This roll is called " + crapsTerms + ".");
        break;
      case 10:
        switch(die1){
          case 5: crapsTerms = "Hard Ten";
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
          default: crapsTerms = "Easy Ten";
            System.out.println("This roll is called " + crapsTerms + ".");
              break;
        }
        break;
      case 11: crapsTerms = "Yo-leven";
        System.out.println("This roll is called " + crapsTerms + ".");
        break;
      case 12: crapsTerms = "Boxcars";
        System.out.println("This roll is called " + crapsTerms+ ".");
        break;
      default: // for situation where the numbers are outside of the range and print invalid
        System.out.println("Invalid entry");
        break;
    }        
  } // end of main
} // end of class    