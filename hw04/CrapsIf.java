//////////////
//// CSE 02 CrapsIf
// Austen Johnson
// September 24th, 2018
// CSE 02 HW 04
///
import java.util.Random; // importing the math class to use math functions in the main method
import java.util.Scanner; // importing the scanner class to use the scanner function to ask for inputs
public class CrapsIf{
  //main method required for every Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //name scanner
    Random numGen = new Random(); //random number generator
    
    System.out.println("Write 'true' if you would like the dice to be selected randomly. Write 'false' if you would like to choose your values."); //Asking the user for their input in how the dice numbers will be
    boolean selectDice = myScanner.nextBoolean(); //Processes users input and assigns the value chosen
    
    int die1,
    die2; // naming the two dice rolls
    String crapsTerms; // naming the craps terms
    
    if (selectDice){ // if the user inputs 'true', then the die will be randomly cast
      die1 = numGen.nextInt(6) + 1; // randomly generates 2 integers from 1-6 for the dice
      die2 = numGen.nextInt(6) + 1;
      System.out.println("The two outcomes from the dice are " + die1 + " and " + die2 + "."); // prints the values of the dice rolls
    }
    else{ // if the user does not input true, this branch is taken
      System.out.print("Enter the value of one die roll: "); // prints prompt for user to input one of the die values
      die1 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
      System.out.print("Enter the value of the other die roll: "); // prints prompt for user to input the other of the die values
      die2 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
    }
    if (die1 >= 1 && die1 <= 6 && die2 >= 1 && die2 <= 6){// uses if and else-if statements to assign the sum of the two rolls to a slang term, then prints a statement stating the term
      if (die1 + die2 == 2){
        crapsTerms = "Snake Eyes";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
      else if (die1 + die2 == 3){
        crapsTerms = "Ace Deuce";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
      else if (die1 + die2 == 4){// used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it different slang terms
        if (die1 == die2){
          crapsTerms = "Hard Four";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
        else{
          crapsTerms = "Easy Four";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
      }
      else if (die1 + die2 == 5){
        crapsTerms = "Fever Five";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
      else if (die1 + die2 == 6){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it a different slang term
        if (die1 == die2){
          crapsTerms = "Hard Six";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
        else{
          crapsTerms = "Easy Six";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
      }
      else if (die1 + die2 == 7){
        crapsTerms = "Seven Out";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
      else if (die1 + die2 == 8){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it a different slang term
        if (die1 == die2){
          crapsTerms = "Hard Eight";
          System.out.println("This roll is called " + crapsTerms+ ".");
        }
        else{
          crapsTerms = "Easy Eight";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
      }
      else if (die1 + die2 == 9){
        crapsTerms = "Nine";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
      else if (die1 + die2 == 10){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it a different slang term
        if (die1 == die2){
          crapsTerms = "Hard Ten";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
        else{
          crapsTerms = "Easy Ten";
          System.out.println("This roll is called " + crapsTerms + ".");
        }
      }
      else if (die1 + die2 == 11){
        crapsTerms = "Yo-leven";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
      else if (die1 + die2 == 12){
        crapsTerms = "Boxcars";
        System.out.println("This roll is called " + crapsTerms + ".");
      }
    }
    else{ // prints 'Invalid entry' when the value is outside of the range of die values
      System.out.println("Invalid entry");
    }  
  } // end of main
} // end of class
    