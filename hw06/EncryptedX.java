///////
// Austen Johnson 
//10/22/18
// CSE 02 HW 6
//// 
import java.util.Scanner;
public class  EncryptedX{

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int gridAmount; //variable for the length of the grid
        int checkRange = 0; //used to check the range of the input, whether it is between 1 and 100
      
        System.out.print("Enter an integer from 1 to 100: ");
        while (!scan.hasNextInt()){// loop to ask again if input if invalid
            System.out.println("That is not an integer.");
            System.out.print("Enter an integer from 1 to 100: ");
            String junk = scan.next(); //dump the entry to a dummy string
        }
          while (scan.hasNextInt()) {
            checkRange = scan.nextInt(); //assigning the input to checkRange to check the range
            if (checkRange<1 || checkRange>100){ //asks user again if input is not in the range of 1 to 100
            System.out.println("That is not in the range of 1 to 100.");
            System.out.print("Enter an integer from 1 to 100: ");
            }
            else if (checkRange >=1 && checkRange <=100){
                break; // if the input is between 1 and 100 it breaks out of the loop
            }
        }
        gridAmount = checkRange; //grid amount is assigned 
        
        
        for (int numRow = 0; numRow<=gridAmount; numRow++){ //printing rows
            for (int numCol = 0; numCol<=gridAmount; numCol++){ //printing columns
                if(numRow == numCol || gridAmount - numCol == numRow)
                  System.out.print(" ");
                else
                  System.out.print("*");
                

            }
            System.out.println(); //prints new row on new line
        }
        
        
        
    }
    
}