//////////////
//// CSE 02 Check
// Austen Johnson
// September 13th, 2018
// CSE 02 Lab 03
///
import java.util.Scanner;
//Using a scanner split a dinner check into a number of ways evenly so that the check is paid in full

public class Check{
  //main method required for every Java program
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); //naming the scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompting the user to put in an amount for the check
    double checkCost = myScanner.nextDouble(); //allows the user to put in a numerical value for the check
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompting the user to intup how much tip they would like to pay
    double tipPercent = myScanner.nextDouble(); //allows the user to input a tip percentage
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: "); //prompting the user to imput how many ways to split the check
    int numPeople = myScanner.nextInt(); //allows the user to input how many ways to split the check
    
    double totalCost; //declaring totalCost
    double costPerPerson; //declaring costPerPerson
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
    totalCost = checkCost * (1 + tipPercent); //
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson; //get dollar amount  
    dimes=(int)(costPerPerson * 10) % 10; //get dimes amount
    pennies=(int)(costPerPerson * 100) % 10; //get pennies amount
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //splits the check evenly between the attendees of dinner



    
    
  } // end of main method
} //end of class